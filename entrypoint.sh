#!/bin/sh
set -e -o pipefail -x

function die () {
  echo "$@"
  exit 1
}

[ -f /etc/pdns/pdns.conf ] || \
  die "No configuration file found at /etc/pdns/pdns.conf. Please, use the standard location."

[ $(grep -E '^launch=gpgsql$' /etc/pdns/pdns.conf | wc -l) -eq 1 ] || \
  die "This image is meant to be used with PostgreSQL backend"

# SQL Setup
#psql postgresql://$PDNS_GPGSQL_USER:$PDNS_GPGSQL_PASSWORD@$PDNS_GPGSQL_HOST/$PDNS_GPGSQL_DBNAME -f /entrypoint/bootstrap.sql


# Extract PDNS_ environment variables to their corresponding config variable
rm -f /pdns.d/00environment.conf

for pdns_env_var in $(printenv | awk -F '=' '/^PDNS_/ { print $1 }' | sort); do
  pdnsvar=$(echo $pdns_env_var | sed -e 's/^PDNS_//' | tr '[:upper:]_' '[:lower:]-')
  pdnsval=$(eval "echo \${${pdns_env_var}}")
  printf "%s=%s\n" $pdnsvar $pdnsval >> /pdns.d/00environment.conf
done

ALLZONES=$(pdnsutil list-all-zones)
for ZONEFILE in "${ZONEDIR:=/zones}"/*; do
  if [ -f "$ZONEFILE" ]; then
    ZONENAME=$(basename "$ZONEFILE")
    if [[ "$ALLZONES" =~ $ZONENAME ]]; then
      continue;
    else
      pdnsutil load-zone $ZONENAME "$ZONEFILE"
      pdnsutil set-meta $ZONENAME SOA-EDIT-API DEFAULT
    fi
  fi
done

exec "$@"
