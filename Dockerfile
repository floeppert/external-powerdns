FROM alpine:3.18

ARG BOOTSTRAP_SQL=schema.pgsql-4.7.x.sql

COPY entrypoint.sh /entrypoint/script
COPY $BOOTSTRAP_SQL /entrypoint/bootstrap.sql

RUN apk add --no-cache pdns==4.7.4-r0 pdns-backend-pgsql==4.7.4-r0 postgresql14-client==14.8-r0 pdns-tools==4.7.4-r0 && \
    mkdir -p /pdns.d

ENV VERSION=4.7

EXPOSE 53 53/udp

ENTRYPOINT [ "/bin/sh", "/entrypoint/script" ]
CMD [ "/usr/sbin/pdns_server" ]
